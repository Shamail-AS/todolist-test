import '../styles/globals.css'
import type { AppProps } from 'next/app'
import ModalContainer from '../components/common/modal-dialog/modal-container'

function MyApp({ Component, pageProps }: AppProps) {
  return <>
    <Component {...pageProps} />
    <ModalContainer></ModalContainer>
  </>
}

export default MyApp
