import React from "react";
import { CircleLoaderSvg } from "../svg/loader";
import styles from './loader.module.scss'


export default function Loader(props: { full?: any, small?: any, smaller?: any, slow?: boolean, nofill?: any, white?: any }) {

    return (

        <div className={`${styles.loader_container} ${props.nofill ? styles.no_fill : ''} ${props.white ? styles.white : ''} ${props.full ? styles.full : ''}`}>
            <div className={`${styles.loader_spin} ${props.slow ? styles.slow : ''} ${props.smaller ? styles.smaller : ''}
              ${props.full ? styles.full : ''} ${props.small ? styles.small : ''} `}>
                <CircleLoaderSvg></CircleLoaderSvg>

            </div>
        </div>
    )
}

export function NothingToSeeHere(props: { full?: any }) {
    return (
        <div className={[styles.nothing_to_see, props.full ? styles.full : ''].join(' ')}>
            {/* <img src="/images/nothing_to_see_here.png"></img> */}
            <p>NO DATA</p>
        </div>
    )
}