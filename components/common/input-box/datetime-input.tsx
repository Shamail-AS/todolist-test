import Timekeeper from 'react-timekeeper';
import MyDateTime from '../../../models/app/MyDateTime';
import { useEffect, useRef, useState } from 'react';
import ModalBox from '../modal-dialog/modal';
import React from 'react';
import ReactDatePicker from 'react-datepicker';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import styles from "./inputs.module.scss";
import "react-datepicker/dist/react-datepicker.css";

interface TimeProps {
    valueChanged: (time_str: string) => void
    label?: string,
    defaultValue?: string
    popupMode?: boolean
    popupShow?: boolean
    onClick?: () => void
    nullable?: any
    readonly?: boolean
    placeholder?: string
    slim?: any
    compact?: any
    alignLeft?:boolean
}

export function TimeInput(props: TimeProps) {
    const time = props.defaultValue || new MyDateTime().LUX_toFormatString("HH:mm");

    const [open, setOpen] = useState(false);
    const [time_str, setTime] = useState(time);
    const [displayTime, setDisplayTime] = useState(props.nullable ? undefined : (props.defaultValue || "-- : --"));

    function timeChanged(new_time: string|null) {
        if (new_time != null) {
            setTime(new_time);
        }
        setDisplayTime(new_time ?? undefined);
        props.valueChanged(new_time ?? "");
    }

    function toggleOpen(e:React.MouseEvent|undefined) {
        if (props.popupMode && props.onClick && !props.readonly) {
            props.onClick();
            e?.preventDefault();
            e?.stopPropagation();
            if (props.popupShow === undefined) {
                setOpen(!open);
            }
        } else {
            setOpen(!open);
        }
    }

    const popupShowing = props.popupMode ? props.popupShow === undefined ? open : props.popupShow : open;

    return (
        <div className={`${styles.inputs_container} ${props.slim ? styles.slim : ''} ${props.popupMode ? styles.as_popup : ''}`}>
            {props.label && (<label>{props.label}</label>)}
            <div className={styles.wrapper}>
                <input readOnly={props.readonly} placeholder={props.placeholder} className={`${styles.timepicker} ${props.compact ? styles.comapct : ''}`} onClick={(e) => toggleOpen(e)} value={displayTime} onChange={() => { }}></input>
                {props.nullable && displayTime && <span onClick={() => timeChanged(null)} className={styles.clear_btn}><FontAwesomeIcon icon={faTimes}></FontAwesomeIcon></span>}
            </div>

            {!props.readonly && (!props.popupMode ?
                <ModalBox show={open} title="What time?">
                    <Timekeeper time={time_str} onChange={(t) => timeChanged(t.formatted24)}
                        hour24Mode={true} onDoneClick={() => setOpen(false)}></Timekeeper>
                </ModalBox>
                :
                <div style={{ position: "absolute", top: 40, right: 0, left:props.alignLeft ? 0 : "auto", display: popupShowing ? "inherit" : "none", zIndex: 10 }}>
                    <Timekeeper time={time_str} onChange={(t) => timeChanged(t.formatted24)}
                        hour24Mode={true} onDoneClick={(t,e) => toggleOpen(e)}></Timekeeper>
                </div>)
            }
        </div>
    )
}

interface DateTimeProps {
    valueChanged: (dt: MyDateTime|null) => void
    label?: string,
    defaultValue?: MyDateTime
    popupMode?: boolean
    popupShow?: boolean
    onClick?: () => void
    nullable?: any
    readonly?: boolean
    placeholder?: string
    withTime?: any
    suffix?: string
    slim?: any
    minDate?: MyDateTime
    maxDate?: MyDateTime
    defaultEod?: boolean
}
export function DateInput(props: DateTimeProps) {
    const date = props.defaultValue || new MyDateTime();

    const [open, setOpen] = useState(false);
    const [my_date, setDate] = useState(props.nullable ? props.defaultValue ? date : null : date);

    function dateChanged(new_val: Date|null) {
        if (new_val == null) {
            setDate(null);
            props.valueChanged(null);
        }
        else {
            const new_dt = new MyDateTime(new_val.toISOString());
            if (my_date == null && props.defaultEod) {
                new_dt.eod();
            }

            setDate(new_dt);
            props.valueChanged(new_dt);
        }

        // // setOpen(false);
        // //THE COMPONENT DOES SOME STATE UPDATE ON DATE SELECT.  SO NEED TO WAIT A BIT
        if (!props.withTime) {
            setTimeout(() => {
                setOpen(false);
            }, 100);
        }
    }

    function toggleOpen() {
        if (props.readonly) return;

        if (props.popupMode && props.onClick) {
            props.onClick();
            if (props.popupShow === undefined) {
                setOpen(!open);
            }
        } else {
            setOpen(!open);
        }
    }

    useEffect(() => {
        if (props.minDate && my_date && my_date.LUX_isPast(props.minDate)) {
            setDate(props.minDate);
        }
        if (props.maxDate && my_date && props.maxDate.LUX_isPast(my_date)) {
            setDate(props.maxDate);
        }
    }, [props.minDate, props.maxDate, my_date])

    const popupRef = useRef<HTMLDivElement>(null);
    useEffect(() => {
        const clicker = (e:any) => {
            if(popupRef.current != null){
                if(!popupRef.current.contains(e.target)){
                    setOpen(false);
                }
            }
            
        }
        document.addEventListener('mouseup', clicker);
        return () => {
            document.removeEventListener('mouseup', clicker);
        }
    },[])

    const popupShowing = props.popupMode ? props.popupShow === undefined ? open : props.popupShow : open;

    return (
        <div className={`${styles.inputs_container} ${props.slim ? styles.slim : ''} ${props.popupMode ? styles.as_popup : ''}`}>
            {props.label && (<label>{props.label}</label>)}
            <div className={styles.wrapper}>
                <input readOnly={props.readonly} placeholder={props.placeholder} className={styles.timepicker} onClick={() => toggleOpen()}
                    value={(props.withTime ? my_date?.LUX_toShortDateTimeString(true) : my_date?.LUX_toShortDateString(false, true)?.concat(props.suffix ? ` ${props.suffix}` : '')) ?? ""} onChange={() => { }}></input>
                {props.nullable && my_date && <span onClick={() => dateChanged(null)} className={styles.clear_btn}><FontAwesomeIcon icon={faTimes}></FontAwesomeIcon></span>}
            </div>


            {!props.popupMode ?
                <ModalBox show={open} title="What date?">
                    <div>
                        <ReactDatePicker minDate={props.minDate?.LUX_getJsDate()} maxDate={props.maxDate?.LUX_getJsDate()} showTimeInput={props.withTime} inline selected={my_date?.LUX_getJsDate()} onChange={(dt: Date) => dateChanged(dt)} ></ReactDatePicker>
                    </div>
                </ModalBox>
                :

                <div ref={popupRef} className={`${styles.date_popup_container} ${props.withTime ? styles.with_time : ''}`} style={{ display: popupShowing ? "inherit" : "none" }}>
                    {/* <div style={{ position: "fixed", top: 0, left: 0, width: "100vw", height: "100vh" }} onClick={() => setOpen(false)}></div> */}
                    <ReactDatePicker minDate={props.minDate?.LUX_getJsDate()} maxDate={props.maxDate?.LUX_getJsDate()} showTimeInput={props.withTime} inline selected={my_date?.LUX_getJsDate()} onChange={(dt: Date) => dateChanged(dt)} ></ReactDatePicker>
                </div>

            }
        </div>
    )
}