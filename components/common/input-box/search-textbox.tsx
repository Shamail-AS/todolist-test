import React, { useEffect, useRef, useState } from "react";
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators'
import { TextBoxType } from "../../../models/app/enums";
import styles from "./inputs.module.scss";

interface props {
    onSearchChanged: (text: string) => void
    placeholder?: string
    translucent?: any
    initial?: string
    withBorder?: any
    label?: string
    err?: string
    value?: string
    required?: boolean
    type?: TextBoxType
    slim?: any
}

export default function SearchTextBox(props: props) {

    const [text, setText] = useState("");
    const sub = useRef(new Subject<string>());

    const { onSearchChanged } = props;
    useEffect(() => {
        sub.current.pipe(debounceTime(500)).subscribe((text_value: string) => {
            onSearchChanged(text_value);
        })
    }, [onSearchChanged])

    function onTextEntered(e: any) {
        const text: string = e.target.value;
        sub.current.next(text);
        setText(text);
    }

    return (
        <div className={`${styles.inputs_container} ${props.slim ? styles.slim : ''} `}>
            {props.label && (<label>{props.label}</label>)}
            <input type={props.type || 'text'}
                className={`${styles.textbox} ${props.translucent ? styles.translucent : ''}`}
                placeholder={props.placeholder || "Search..."}
                value={text}
                onChange={(e) => onTextEntered(e)}></input>
        </div>
    )
}