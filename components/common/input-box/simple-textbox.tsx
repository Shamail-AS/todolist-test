import React, { HTMLAttributes, useEffect, useRef, useState } from "react";
import { TextBoxType } from "../../../models/app/enums";
import styles from "./inputs.module.scss";

interface props {
    onValueChanged?: (text: string) => void
    placeholder?: string
    initial?: string | number
    noBorder?: any
    label?: string
    err?: string
    fieldValue?: string | number
    type?: TextBoxType
    readonly?: any
    greyed?: any
    multiline?: any
    fill?: any,
    slim?: any,
    required?: boolean
    minLength?: number
    maxLength?: number
    minVal?: number
    maxVal?: number
    formatRegex?: RegExp
    nonzero?: boolean
    decimals?: number
    prefix?: string
    style?: React.CSSProperties
    innerStyle?: React.CSSProperties
}

export class RegExFormats {
    static posint = /[^0-9]*/g;
    static int = /[^-0-9]*/g;
    static posreal = /[^\.0-9]*/g;
    static real = /[^\.-0-9]*/g;
    static phone = /[^\+()0-9]*/g;
    static accounts = /[^-0-9]*/g;
    static alphabets = /[^a-zA-Z\s]*/g
    static email = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
    static single_line = /[\n\r]/g;
}
export default function SimpleTextBox(props: props) {
    const [text, setText] = useState((props.prefix ?? "") + (props.initial?.toString() ?? ""));
    const _alreadyChecked = useRef(false);

    function whenTextChanged(text: string) {

        if (props.prefix && !text.includes(props.prefix)) {
            text = props.prefix + text;
        }
        props.onValueChanged?.(text);

        !props.fieldValue && setText(text);
    }

    function onKeyPress(e: React.KeyboardEvent<HTMLInputElement> | React.KeyboardEvent<HTMLTextAreaElement>) {
        //console.log("onKeyPress", e.currentTarget.value);

        if (e.key == "Enter" && props.formatRegex?.toString() == RegExFormats.single_line.toString()) {
            e.preventDefault();
            return;
        }
    }

    function onKeyDown(e: React.KeyboardEvent<HTMLInputElement> | React.KeyboardEvent<HTMLTextAreaElement>) {

        //console.log("onKeyDown", e.currentTarget.value);

        if (e.key?.length > 1 || e.metaKey || e.ctrlKey) {
            return;
        }

        const new_val = e.currentTarget.value + e.key;
        if (props.maxLength != null && new_val.length > props.maxLength) {
            e.preventDefault();
            return;
        }

        let filtered = filterText(new_val);
        if (new_val.length != filtered.length) {
            e.preventDefault();
            return;
        }
        _alreadyChecked.current = true;
    }

    function onInputChange(e: React.FormEvent<HTMLInputElement>) {
        let text: string = e.currentTarget.value;
        if (_alreadyChecked.current == false) {
            let filtered = filterText(text);
            if (text.length != filtered.length) {
                e.preventDefault();
                e.stopPropagation();
                return;
            }
        }
        _alreadyChecked.current = false;

        //console.log("onInputChange", text);
        whenTextChanged(text);
    }

    function filterText(input: string) {
        let text: string = input;
        //console.log("Filter text", text);
        if (props.formatRegex) {
            text = text.replace(props.formatRegex, '');
        }
        return text;
    }

    function onLoseFocus(e: React.FocusEvent<HTMLInputElement> | React.FocusEvent<HTMLTextAreaElement>) {


        const max_safe_number = 10000000000000;
        let text: string = e.currentTarget.value;

        //console.log("onLoseFocus", text);
        let final_text: string = text;
        if (props.type == TextBoxType.Numeric) {
            final_text = final_text.replace(props.formatRegex || RegExFormats.posint, '');
            let text_num = Number(final_text);

            if (text_num > (props.maxVal ?? max_safe_number)) {
                text_num = props.maxVal ?? max_safe_number;
            }
            if (text_num < (props.minVal ?? 0)) {
                text_num = props.minVal ?? 0;
            }
            if (props.nonzero && text_num <= 0) {
                text_num = 1;
            }
            if (props.decimals) {
                final_text = text_num.toFixed(props.decimals);
            }
            else {
                final_text = text_num.toString();
            }

        }
        else if (props.multiline && props.formatRegex?.toString() == RegExFormats.single_line.toString()) {
            final_text = filterText(final_text);
        }

        e.currentTarget.value = final_text;

        //only trigger on lose focus update where changes can happen after lose focus
        if (props.type == TextBoxType.Numeric || props.multiline)
            whenTextChanged(e.currentTarget.value);

    }

    useEffect(() => {
        setText(props.fieldValue?.toString() ?? "")
    }, [props.fieldValue])

    const fill = props.fill ? styles.fill : '';
    const slim = props.slim ? styles.slim : '';
    const grey = props.greyed ? styles.greyed : '';
    const show_value = text ?? "";

    return (
        <div className={`${styles.inputs_container} ${fill} ${slim}`} style={props.style}>
            {props.label && (<label>{props.label}</label>)}
            {props.err && props.err.length > 0 && (<p className={styles.field_err}>{props.err}</p>)}
            {props.required && show_value.length < (props.minLength ?? 1) && (<p className={styles.field_err}>required field</p>)}
            {props.required && props.nonzero && Number(show_value) == 0 && (<p className={styles.field_err}>can&apos;t be zero</p>)}
            {props.required && props.type == TextBoxType.Email && !RegExFormats.email.test(show_value) && (<p className={styles.field_err}>ensure correct email format email@example.com</p>)}
            {props.multiline ?
                <div className={`${styles.text_area} ${props.noBorder ? styles.no_border : ''} ${grey}`}>
                    <textarea readOnly={props.readonly} rows={6} placeholder={props.placeholder || ""} value={show_value}
                        onKeyDown={e => onKeyDown(e)}
                        onKeyPress={e => onKeyPress(e)}
                        onBlur={e => onLoseFocus(e)}
                        onChange={(e) => whenTextChanged(e.target.value)}></textarea>
                </div>
                :
                <input readOnly={props.readonly} type={props.type || 'text'} style={props.innerStyle}
                    className={`${styles.textbox} ${props.noBorder ? styles.no_border : ''} ${grey}`} placeholder={props.placeholder || ""} value={show_value}
                    onKeyDown={e => onKeyDown(e)}
                    onBlur={e => onLoseFocus(e)}
                    // onChange={(e) => whenTextChanged(e.target.value)}
                    onInput={e => onInputChange(e)}></input>}

        </div>
    )

}