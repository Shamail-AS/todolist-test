import { useEffect, useState } from 'react';
import Select, { components } from 'react-select';
import Loader from '../loader/loader';
import styles from "./inputs.module.scss";

export interface IMyDropdownOption {
    id: string | number, name: string
}
interface MyDropdownProps {
    options: IMyDropdownOption[]
    inputId?: string
    selectionChanged: (entity: IMyDropdownOption) => void
    initial?: string | number
    label?: string
    disabled?: any
    loading?: boolean
    nullable?: any
    slim?: any
    matchOnName?: boolean
    required?: boolean
}
export function MyDropdown(props: MyDropdownProps) {

    let options = props.options?.map(entity => {
        return {
            value: entity.id,
            label: entity.name
        }
    })

    const defaultVal = props.nullable && !props.initial ? null :
        options.filter(x => props.matchOnName ? x.label == (props.initial || x.label) : x.value == (props.initial || x.value))[0];

    const [selected, setSelected] = useState(defaultVal?.value);
    useEffect(() => {
        setSelected(defaultVal?.value);
    },[defaultVal])

    const Option = (props:any) => {
        return (
            <div style={{ display: 'flex', alignItems: 'center' }}>
                <components.Option {...props} />
            </div>
        );
    };

    function entitySelected(entity_id: any) {
        const selected_entity = props.options?.find(c => c.id == entity_id.toString());
        setSelected(selected_entity?.id);

        if (selected_entity != null)
            props.selectionChanged(selected_entity);

    }

    //const rn = Math.floor(Math.random() * 1000);
    const inputId = (props.inputId || "basic");
    return (
        <div className={`${styles.inputs_container} ${props.slim ? styles.slim : ''} `}>
            {props.label && (<label>{props.label}</label>)}
            {props.required && (selected?.toString() ?? "").length == 0 && (<p className={styles.field_err}>required field</p>)}
            {props.loading ? <Loader small></Loader> : <Select
                inputId={inputId} instanceId={props.inputId + Date.now().toString()}
                closeMenuOnSelect={true}
                components={{ Option }}
                onChange={(e) => entitySelected((e?.value ?? -1))}
                defaultValue={defaultVal}
                options={options}
                isDisabled={!!props.disabled}
                isLoading={props.loading}
                loadingMessage={() => "Loading..."}
                maxMenuHeight={200}
            >

            </Select>}
        </div>
    )
}