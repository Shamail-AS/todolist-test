import { useState } from "react";
import { CheckButton, RadioButton } from "../buttons/check-radio-btn";
import styles from "./inputs.module.scss";

interface IProps {
    on?: boolean
    initial?: boolean
    onToggle: (new_state: boolean | null) => void
    label?: string | JSX.Element,
    subLabel?: string,
    rightAddOn?: string,
    primaryAddOn?: any,
    externalLabel?: boolean,
    triMode?: boolean,
    slim?: any,
    required?: any
    disabled?: boolean
}
export function CheckInput(props: IProps) {
    return (
        <div className={`${styles.inputs_container} ${props.slim ? styles.slim : ''} `}>
            {props.label && props.externalLabel && (<label>{props.label}</label>)}
            <CheckButton {...props}></CheckButton>
        </div>
    )
}

export function RadioInput(props: IProps) {
    const radioprops: IProps = { ...props, label: props.externalLabel ? undefined : props.label }

    const [isOn, setOn] = useState(props.initial ?? props.on);
    let on = props.on !== undefined ? props.on : isOn
    
    radioprops.on = on;

    const toggleButton = () => {
        if(props.disabled) return;
        
        const new_state = !on;
        props.onToggle(new_state);
        setOn(new_state);
    }
    return (
        <div className={`${styles.inputs_container} ${props.slim ? styles.slim : ''}`} style={{ flexDirection: "row", alignItems: "center" }} onClick={ toggleButton }>
            <RadioButton {...radioprops}></RadioButton>
            {props.label && props.externalLabel && (
                <div className={styles.mid_text_section}>
                    <label>{props.label}</label>
                    {props.subLabel && <span className={`${props.required ? styles.danger : ''}`}>{props.subLabel}</span>}
                </div>
            )}
            {props.rightAddOn && (
                <div className={`${styles.right_addon_section} ${props.primaryAddOn ? styles.primary_addon : ''}`}>
                    <span>{props.rightAddOn}</span>
                </div>
            )}
        </div>
    )
}