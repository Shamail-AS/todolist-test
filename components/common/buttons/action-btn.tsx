import React, { useEffect } from "react";
import ControlService, { IModalControlMessage } from "../../../services/control.service";
import Loader from "../loader/loader";
import btnStyles from './buttons.module.scss';

interface IProps {
    primary?: any
    secondary?: any
    tertiary?: any
    danger?: any
    slim?: any
    fat?: any
    flat?: any
    fill?: any
    large?: any
    long?:any;
    hollow?: any
    noBorder?: any
    forInput?:any
    onClick?: () => void
    disabled?: boolean
    loading?: boolean,
    title?: string,
    noBubble?: boolean,
    inactive?: boolean,
    children: any
}
export default function ActionButton(props: IProps) {

    const pri = props.primary ? btnStyles.primary : '';
    const sec = props.secondary ? btnStyles.secondary : '';
    const ter = props.tertiary ? btnStyles.tertiary : '';
    const slim = props.slim ? btnStyles.slim : props.fat ? btnStyles.fat : '';
    const flat = props.flat ? btnStyles.flat : '';
    const danger = props.danger ? btnStyles.danger : '';
    const fill = props.fill ? btnStyles.fill : '';
    const large = props.large ? '' : btnStyles.btn_sml;
    const long = props.long ? btnStyles.btn_long : '';
    const disabled = props.disabled ? btnStyles.disabled : '';
    const grey = props.inactive ? btnStyles.inactive : '';
    const hollow = props.hollow ? btnStyles.hollow : '';
    const no_border = props.noBorder ? btnStyles.no_border : '';
    const no_pointer = !props.onClick ? btnStyles.no_pointer : '';
    const for_input = props.forInput ? btnStyles.for_input : '';

    const classStr = `${btnStyles.quick_action} ${large} ${long} ${pri} ${sec} ${ter} ${danger} ${flat} ${fill} ${slim} ${disabled} ${grey} ${hollow} ${no_border} ${no_pointer} ${for_input}`

    const is_title_html = props.title?.startsWith("<");
    const title_text = is_title_html ? "API call wasn't successful. Can't display HTML error message in tooltip." : props.title?.trim() ?? ""

    useEffect(() => {
        const is_title_html = props.title?.startsWith("<");

        if (is_title_html) {
            const msg: IModalControlMessage = {
                type: "modal",
                payload: {
                    title: "Action Button HTML API Error Preview",
                    content: <iframe width="100%" height="600" srcDoc={props.title}></iframe>,
                    show: true,
                    wide: true
                }
            }

            ControlService.broadcast(msg);
        }
    }, [props.title])




    return (
        <div title={title_text} className={classStr} onClick={(e) => {

            if (props.onClick && !props.disabled && !props.loading)
                props.onClick();

            if (props.noBubble) {
                e.preventDefault();
                e.stopPropagation();
            }
        }}>
            {props.loading ? <Loader smaller nofill></Loader> : props.children}
        </div>

    )
}
