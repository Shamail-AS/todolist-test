import { faCheck, faCross, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react"
import { CheckStates } from "../../../models/app/enums";
import styles from './buttons.module.scss';

interface IProps {
    on?: boolean
    initial?: boolean
    onToggle: (new_state: boolean | null) => void
    label?: string | JSX.Element,
    text?: string
    triMode?: boolean
    readonly?: boolean
    disabled?: boolean
    title?: string
}
export function RadioButton(props: IProps) {

    const [isOn, setOn] = useState(props.initial ?? props.on);
    let on = props.on !== undefined ? props.on : isOn

    const toggleButton = () => {
        if (props.disabled) return;

        const new_state = !on;
        props.onToggle(new_state);
        setOn(new_state);
    }
    const dis_cls = props.disabled ? styles.disabled : '';
    return (
        <div className={`${props.label ? styles.label_control : ''}`}>
            <div className={`${styles.radio} ${on ? styles.on : ''} ${props.label ? styles.small : ''} ${dis_cls}`} onClick={toggleButton}>
                <div></div>
            </div>
            {props.label && <span>{props.label}</span>}
        </div>
    )
}

export function CheckButton(props: IProps) {


    const [checkState, setCheckState] = useState(props.on ? CheckStates.Checked : props.initial === true ? CheckStates.Checked : CheckStates.None);
    let status = props.on !== undefined ? CheckStates.Checked : checkState

    const tri_states = [CheckStates.None, CheckStates.Checked, CheckStates.Crossed];
    const duo_states = [CheckStates.None, CheckStates.Checked];

    const cycleCheckState = () => {
        if (props.readonly || props.disabled) return;
        const status_arr = props.triMode ? tri_states : duo_states;
        const index = status_arr.findIndex(x => x == checkState);
        const next_index = (index + 1) % status_arr.length;
        const next_status = status_arr[next_index];

        setCheckState(next_status);
        props.onToggle(next_status == CheckStates.None ? props.triMode ? null : false : next_status == CheckStates.Checked);
    }

    const dis_cls = props.disabled ? styles.disabled : '';
    return (
        <div className={styles.radio_container}>
            <div className={`${props.label ? styles.label_control : ''}`}>
                <div className={`${styles.checkbox} ${checkState != CheckStates.None ? styles.check_on : ''} ${dis_cls}`} onClick={cycleCheckState}>
                    <div>
                        <FontAwesomeIcon icon={status == CheckStates.Crossed ? faTimes : faCheck}></FontAwesomeIcon>
                    </div>
                </div>
                {props.label && <span>{props.label}</span>}
            </div>

            {props.text && <span>{props.text}</span>}
        </div>

    )
}