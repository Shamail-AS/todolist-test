import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWindowClose } from "@fortawesome/free-solid-svg-icons";

import styles from "./modal.module.scss";
interface props {
    show: boolean
    title: string,
    compact?:any,
    full_screen?: any
    wide?: any
    children?: any
    onClose?: () => void
}

export default function ModalBox(props: props) {
    const { full_screen, title, wide } = props;

    if (!props.show) {
        return <></>;
    }

    function wrapperClickHandler(was_wrapper_clicked: boolean) {
        if (was_wrapper_clicked && props.onClose) {
            props.onClose();
        }
    }

    const full_cls = full_screen ? styles.full : '';
    const wide_cls = wide ? styles.wide : '';
    return (
        <div className={`${styles.modal_wrapper} ${full_cls} ${wide_cls}`} onClick={(e) => wrapperClickHandler(e.target === e.currentTarget)}>
            <div className={props.compact ? '' : styles.modal_positioner}>
                <div className={styles.modal_container}>
                    <div className={styles.header}>{title}
                        {props.onClose && <div className={styles.close} onClick={() => props.onClose && props.onClose()}>
                            <FontAwesomeIcon icon={faWindowClose}></FontAwesomeIcon>
                        </div>}
                    </div>
                    <div className={styles.body}>
                        {props.children}
                    </div>
                </div>
            </div>
        </div>
    )
}