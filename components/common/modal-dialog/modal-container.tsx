import { useEffect, useState } from "react";
import ControlService, { IModalControlMessage } from "../../../services/control.service";
import ModalBox from "./modal";

export default function ModalContainer(props:any){

    const [open, setOpen] = useState(false);
    const [wide, setWide] = useState(false);
    const [content, setContent] = useState<JSX.Element>();
    //let content:JSX.Element = null;
    const [title, setTitle] = useState("");

    useEffect(() => {

        const sub = ControlService.attachListener((msg:IModalControlMessage) => {
            if(msg.type == "modal"){
                setContent(msg.payload.content);
                setTitle(msg.payload.title);
                setOpen(msg.payload.show);
                setWide(msg.payload.wide ?? false)
            }
        });

        return () => {
            sub.unsubscribe();
        }

    }, [])

    return (
        <ModalBox {...props} title={title} wide={wide} show={open} onClose={() => setOpen(false)} >
            {content}
        </ModalBox>
    )
}