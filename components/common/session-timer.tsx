import { useEffect, useRef, useState } from "react"
import Constants from "../../constants";
import styles from "./session-timer.module.scss";

export function SessionTimer({onSessionExpire}:{onSessionExpire:() => void}) {

    const seconds = useRef(0);
    const [sessionExpired, setSessionExpired] = useState(false);
    const [expire_warning, setExpireWarning] = useState("");

    const EXPIRY_SECONDS = Constants.SESSION_MINUTES_EXPIRY * 60;
    const WARN_SECONDS = 5;

    function onMouseMove(e: MouseEvent) {
        seconds.current = 0;
        setExpireWarning("");
    }
    useEffect(() => {
        document.addEventListener("mousedown", onMouseMove);
        document.addEventListener("pointerdown", onMouseMove);
        const interval = setInterval(() => {
            seconds.current = seconds.current + 1;

            if (seconds.current >= (EXPIRY_SECONDS - WARN_SECONDS) && seconds.current < EXPIRY_SECONDS) {
                const warn_msg = "Session will expire in " + (EXPIRY_SECONDS - seconds.current) + ". Click anywhere to reset";
                setExpireWarning(warn_msg);
            }

            if (seconds.current >= EXPIRY_SECONDS) {
                console.log("session expired");
                setSessionExpired(true);
                clearInterval(interval);
                document.removeEventListener("mousedown", onMouseMove);
                document.removeEventListener("pointerdown", onMouseMove);
                onSessionExpire();
            }
        }, 1000);

        return () => {
            document.removeEventListener("mousedown", onMouseMove);
            document.removeEventListener("pointerdown", onMouseMove);
            clearInterval(interval);
        }
    }, [EXPIRY_SECONDS, WARN_SECONDS, seconds])
    const show_cls = sessionExpired || expire_warning.length > 0 ? styles.show : '';
    const warn_cls = expire_warning.length > 0 && !sessionExpired? styles.warn : '';
    return <div className={`${styles.timer_wrapper} ${show_cls} ${warn_cls}`}>{sessionExpired ? "Session Expired. Please refresh." : expire_warning.length > 0 ? expire_warning : null}</div>
}