import React, { useEffect, useState } from 'react'
import styles from './error-handler.module.scss';

interface IProps {
  alertType?: string
  alertMsg: string
  showAlert?: boolean
}
function ErrorHander(props: IProps) {

  const [show, setShow] = useState(props?.showAlert || false);


  useEffect(() => {
    setShow(props.showAlert??false);
  },[props])


  const is_html = props.alertMsg.startsWith("<");
  const [showFrame, setShowFrame] = useState(false);

  if (show) {
    if(is_html){
      return (
        <section className={styles.wrapper} onClick={() => setShowFrame(!showFrame)}>
          {/* <Alert style={{ marginTop: '30px' }} variant={props?.alertType || "danger"} onClose={() => setShow(false)} dismissible> */}
            <span style={{ whiteSpace: "pre-line" }} > Server rejected the request. Please try again later. If this issue occurs again, please contact your system administrator.</span>
          {/* </Alert> */}
          {showFrame && <iframe width="100%" height="300px" srcDoc={props.alertMsg}></iframe>}
        </section>
  
      )
    }
    else{
      return (
        <section className={styles.wrapper}>
          {/* <Alert style={{ marginTop: '30px' }} variant={props?.alertType || "danger"} onClose={() => setShow(false)} dismissible> */}
            <span style={{ whiteSpace: "pre-line" }}> {props?.alertMsg}</span>
          {/* </Alert> */}
        </section>
  
      )
    }
    
  } else {
    return <></>
  }
}

export default ErrorHander
