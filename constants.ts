export default class Constants {
  
    static SUPPORTED_LANGS = ["English", "Urdu"]
    static SUPPORT_EMAIL = "support.todo@shamail.com"
    static SESSION_MINUTES_EXPIRY = 15;
    static BUILD_VERSION = "0.3.5";
}
