import { DateTime } from 'luxon';

export default class MyDateTime {
    value?: string
    lux: DateTime

    constructor(val?: string) {
        this.value = val;
        this.lux = val ? DateTime.fromISO(val) : DateTime.local();
    }
    static FromLuxUnix(unix: number) {
        const dt = new MyDateTime(unix.toString());
        dt.lux = DateTime.fromMillis(unix);
        return dt;
    }
    static FromLuxUTC(utc_iso_time: string) {
        const dt = new MyDateTime(utc_iso_time);
        dt.lux = DateTime.fromISO(utc_iso_time);
        return dt;
    }
    static FromLuxDate(date_str: string) {
        const date_parts = date_str.substr(0,10);
        const dt = new MyDateTime(date_parts);
        dt.lux = DateTime.fromFormat(date_parts,"yyyy-MM-dd");
        return dt;
    }

    static LUX_fromShortDateTimeStringJava(date_str: string, asUTC: boolean = true, noT: boolean = true) {
        const dt = new MyDateTime(date_str);
        if (noT) {
            dt.lux = asUTC ? DateTime.fromFormat(date_str+"+00:00", "yyyy-MM-dd HH:mm:ss.SZZ") : DateTime.fromFormat(date_str,"yyyy-MM-dd HH:mm:ss.S");
        }
        else{
            dt.lux = asUTC ? DateTime.fromFormat(date_str+"+00:00", "yyyy-MM-dd'T'HH:mm:ss.SZZ") : DateTime.fromFormat(date_str,"yyyy-MM-dd'T'HH:mm:ss.S");
        }
        return dt;
    }

    static LUX_Format(date_text: string|number, format: string) {
        if(typeof date_text === 'number'){
            return this.FromLuxUnix(date_text).lux.toFormat(format);
        }
        else
            return DateTime.fromFormat(date_text,"yyyy-MM-dd").toFormat(format);
    }

    addDays(days: number) {
        this.lux = this.lux.plus({
            days
        });
        return this;
    }

    eod() {
        this.lux = this.lux.endOf('day');
        return this;
    }

    sod() {
        this.lux = this.lux.startOf('day');
        return this;
    }

    LUX_isPast(compared_to?: MyDateTime) {
       return this.lux < (compared_to?.lux ?? DateTime.local())
    }
    LUX_isFuture(compared_to?: MyDateTime) {
        return this.lux > (compared_to?.lux ?? DateTime.local())
     }

    LUX_fromNowStr() {
       return this.lux.minus({
           seconds:1
       }).toRelative();
    }

    LUX_minutesFromNow() {
        return Math.abs(this.lux.diffNow("minutes").minutes);
    }

    // toFormatString(format: string) {
    //     return this.date_field.format(format);
    // }

    LUX_toFormatString(format: string) {
        return this.lux.toFormat(format);
    }

    // toFormatUTCString(format: string) {
    //     const dt = moment.utc(this.date_field ?? {});
    //     return dt.format(format);
    // }

    LUX_toFormatUTCString(format: string) {
        const dt = this.lux.toUTC();
        return dt.toFormat(format);
    }

    // toShortDateString(toUTC?: boolean, for_ui: boolean = false) {
    //     if (toUTC) return this.toFormatUTCString(for_ui ? "DD/MM/YYYY" : "YYYY-MM-DD");
    //     return this.toFormatString(for_ui ? "DD/MM/YYYY" : "YYYY-MM-DD");
    // }
    LUX_toShortDateString(toUTC?: boolean, for_ui: boolean = false) {
        // if (toUTC) return this.lux.toUTC().toFormat(for_ui ? "yy/MM/yyyy" : "yyyy-MM-dd");
        if (toUTC) return this.lux.toFormat(for_ui ? "yy/MM/yyyy" : "yyyy-MM-dd");
        return this.lux.toFormat(for_ui ? "dd/MM/yyyy" : "yyyy-MM-dd");
    }

    LUX_toShortDateStringHumanFriendly() {
        return this.LUX_toFormatString("dd-MMM-yyyy");
    }

    LUX_toShortDateTimeString(for_ui: boolean = false, ignore_value: boolean = false) {
        // if (!for_ui) {
        //     return this.lux.toUTC().toFormat("yyyy-MM-dd HH:mm:ss");
        // }
        if (!for_ui) {
            return this.lux.toFormat("yyyy-MM-dd HH:mm:ss");
        }
        if (this.value == null && !ignore_value) return "";
        return this.lux.toFormat("dd/MM/yyyy HH:mm:ss");
    }

    LUX_toShortDateTimeStringJava(toUTC?: boolean, noT?: boolean) {
        // if (this.value == null) return ""
        if (noT) {
            // return toUTC ? this.LUX_toFormatUTCString("yyyy-MM-dd HH:mm:ss.SSS") : this.LUX_toFormatString("yyyy-MM-dd HH:mm:ss.SSS");
            return toUTC ? this.LUX_toFormatString("yyyy-MM-dd HH:mm:ss.SSS") : this.LUX_toFormatString("yyyy-MM-dd HH:mm:ss.SSS");
        }
        // return toUTC ? this.LUX_toFormatUTCString("yyyy-MM-dd'T'HH:mm:ss.SSS") : this.LUX_toFormatString("yyyy-MM-dd'T'HH:mm:ss.SSS");
        return toUTC ? this.LUX_toFormatString("yyyy-MM-dd'T'HH:mm:ss.SSS") : this.LUX_toFormatString("yyyy-MM-dd'T'HH:mm:ss.SSS");
    }
    LUX_toShortDateTimeStringJavaUTC(toUTC?: boolean, noT?: boolean) {
        
        if (noT) {
            return toUTC ? this.LUX_toFormatUTCString("yyyy-MM-dd HH:mm:ss.SSS") : this.LUX_toFormatString("yyyy-MM-dd HH:mm:ss.SSS");
            // return toUTC ? this.LUX_toFormatString("yyyy-MM-dd HH:mm:ss.SSS") : this.LUX_toFormatString("yyyy-MM-dd HH:mm:ss.SSS");
        }
        return toUTC ? this.LUX_toFormatUTCString("yyyy-MM-dd'T'HH:mm:ss.SSS") : this.LUX_toFormatString("yyyy-MM-dd'T'HH:mm:ss.SSS");
        // return toUTC ? this.LUX_toFormatString("yyyy-MM-dd'T'HH:mm:ss.SSS") : this.LUX_toFormatString("yyyy-MM-dd'T'HH:mm:ss.SSS");
    }

    LUX_getJsDate() {
        return this.lux.toJSDate();
    }
    LUX_getISOString() {
        return this.lux.toISO();
    }
}