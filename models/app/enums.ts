export enum FileType {
    Image = "image",
    Video = "video",
    File = "file"
}

export enum TextBoxType {
    Text = 'text',
    Email = 'email',
    Numeric = 'number',
    Password = 'password',
    Phone = 'tel'
}

export enum CheckStates {
    Checked = "checked",
    Crossed = "crossed",
    None = "none",
}