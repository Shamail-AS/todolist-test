import { ILoginResponse } from "../api/response"

export default class AppUser {
    id?: string
    name: string
    email?: string
   
    constructor(data: ILoginResponse) {
        
        this.id = data.id
        this.name = data.name
        this.email = data.email
       
    }
}

