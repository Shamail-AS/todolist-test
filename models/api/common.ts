export interface IApiProvider{
    doGet:(url:string) => Promise<any>
    doPost:(url:string, payload:any) => Promise<any>
    doDelete:(url:string) => Promise<any>
    logout:(email:string) => Promise<any>
}