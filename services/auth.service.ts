import AppUser from "../models/app/user.model";
import StorageService from "./storage.service";

export default class AuthService {

    private static _user: AppUser | null = null;

    public static isLoggedIn(): boolean {
        try {
            return false;
        }
        catch (er) {
            return false;
        }
    }

    public static login(user: AppUser) {

        try {
            StorageService.setInStorage("auth_user", this._user);
            this._user = user
        }
        catch (er) {
            console.log("Couldn't login user.", er);
            this._user = null;
        }
    }

    public static logout() {

        try {
            StorageService.clearStorage();
            StorageService.clearSession();
            this._user = null;
        }
        catch (er) {
            console.log("Couldn't logout user.", er);
        }
    }

    public static loadUser(): AppUser | null {

        try {
            let user = StorageService.getFromStorage<AppUser>("auth_user");
            return user;
        }
        catch (er) {
            return null;
        }
    }

}