import Logger from "./log.service";

export default class StorageService {

    private static _log = new Logger("StorageService");

    private static getStorage() {
        return localStorage;
    }

    private static getSession() {
        return sessionStorage;
    }

    static getFromStorage<T>(key: string): any {
        try {
            let json = this.getStorage().getItem(key);
            const val = JSON.parse(json as string);
            return val as T;
        }
        catch (err) {
            this._log.error(err);
            return null;
        }

    }

    static setInStorage(key: string, value: any) {
        try {
            let json = JSON.stringify(value);
            this.getStorage().setItem(key, json);
        }
        catch (err) {
            this._log.error(err);
        }

    }

    static getFromSession<T>(key: string) {

        try {
            let json = sessionStorage.getItem(key);
            const val = JSON.parse(json as string);
            return val as T;
        }
        catch (err) {
            this._log.error(err);
            return null;
        }
    }
    static setInSession(key: string, value: any) {
        try {
            let json = JSON.stringify(value);

            sessionStorage.setItem(key, json);
        }
        catch (err) {
            this._log.error(err);
        }

    }
    static removeFromSession(key: string) {
        try {
            sessionStorage.removeItem(key);
        }
        catch (err) {
            this._log.error(err);
        }

    }
    static clearStorage() {
        this.getStorage().clear();
    }
    static clearSession() {
        this.getSession().clear();
    }

    static async getFromIndexedDB(key: string): Promise<any> {


        const promise = new Promise((success, error) => {
            let openRequest = indexedDB.open("store", 1);
            openRequest.onupgradeneeded = function () {
                // triggers if the client had no database
                // ...perform initialization...
                let db = openRequest.result;
                if (!db.objectStoreNames.contains('images')) { // if there's no "books" store
                    db.createObjectStore('images', { keyPath: 'key' }); // create it
                    success(null);
                }
            };

            openRequest.onerror = function () {
                console.error("Error", openRequest.error);
                error(openRequest.error?.message);
            };

            openRequest.onsuccess = function () {
                let db = openRequest.result;
                // continue working with database using db object
                // db.onversionchange = function () {
                //     db.close();
                //     alert("Database is outdated, please reload the page.")
                // };

                const images_store = db.transaction("images", "readonly").objectStore("images");
                let request = images_store.get(key);
                request.onsuccess = (event) => {
                    success(request.result?.["value"]);
                    db.close();
                }
              
            };
        });

        return promise;

    }


    static async setInIndexedDB(key: string, value: any): Promise<string> {


        const promise = new Promise<string>((success, error) => {
            let updateRequest = indexedDB.open("store", 1);
            updateRequest.onupgradeneeded = function () {
                // triggers if the client had no database
                // ...perform initialization...
                let db = updateRequest.result;
                if (!db.objectStoreNames.contains('images')) { // if there's no "books" store
                    const store = db.createObjectStore('images', { keyPath: 'key' }); // create it
                    store.transaction.oncomplete = function (event) {
                        // Store values in the newly created objectStore.
                        const images_store = db.transaction("images", "readwrite").objectStore("images");
                        const obj = {
                            key,
                            value
                        }
                        const request = images_store.put(obj)
                        request.onsuccess = (event) => {
                            success("");
                        };
                    }
                }


            };

            updateRequest.onerror = function () {
                console.error("Error", updateRequest.error);
                error(updateRequest.error?.message);
            };

            updateRequest.onsuccess = function () {
                let db = updateRequest.result;
                // continue working with database using db object
                // db.onversionchange = function () {
                //     db.close();
                //     alert("Database is outdated, please reload the page.")
                // };

                const images_store = db.transaction("images", "readwrite").objectStore("images");
                const obj = {
                    key,
                    value
                }
                const request = images_store.put(obj)
                request.onsuccess = (event) => {
                    success("");
                    db.close();
                };
            };
        });

        return promise;


    }


}