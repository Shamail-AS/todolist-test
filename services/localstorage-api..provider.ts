import { IApiProvider } from "../models/api/common";
import StorageService from "./storage.service";

export class LocalStorageApiProvider implements IApiProvider{

    doGet: (url: string) => Promise<any> = (url:string) => {
        return Promise.resolve(null);
    };

    doPost: (url: string, payload: any) => Promise<any> = (url:string, payload: any) => {
        return Promise.resolve(null);
    };

    doDelete: (url: string) => Promise<any> = (url:string) => {
        return Promise.resolve(null);
    };

    logout: (email: string) => Promise<any>= (url:string) => {
        return Promise.resolve(null);
    };

}