import { Subject, Subscription } from 'rxjs';

export default class ControlService {

    private static _sub = new Subject<IControlMessage>();
    public static broadcast(msg: IControlMessage) {
        console.log(msg);
        this._sub.next(msg);
    }

    public static attachListener(listener: (msg: IControlMessage) => void): Subscription {
        // console.log("registered listener");
        return this._sub.subscribe(new_msg => {
            listener(new_msg);
        });
    }

}

export interface IControlMessage {
    type: any
    payload: any
}
export interface IModalControlMessage extends IControlMessage {
    type: "modal"
    payload: { title: string, content: JSX.Element, show: boolean, wide?:boolean }
}