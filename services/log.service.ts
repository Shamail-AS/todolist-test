export default class Logger {
    
    private _out = console;
    private _prefix: string;
    constructor(prefix: string) {
        this._prefix = prefix;
    }
    public error(msg: any) {
        if (!!msg && typeof msg != "string") {
            this._out.error(this._prefix, msg);
        }
        else
            this._out.error(`${this._prefix}_ERROR_::${msg}`);
    }
    public warn(msg: any) {
        if (!!msg && typeof msg != "string") {
            this._out.warn(this._prefix, msg);
        }
        else
            this._out.warn(`${this._prefix}_WARN_::${msg}`);
    }
    public log(msg: any) {
        if (!!msg && typeof msg != "string") {
            this._out.log(this._prefix, msg);
        }
        else {

            this._out.log(`${this._prefix}_LOG_::${msg}`);
        }
    }
}