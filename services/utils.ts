import { FileType } from "../models/app/enums";

export class ArrayUtils {
    static takeLast(array: any[], count: number): any[] {
        if (!Array.isArray(array)) return [];
        return array.slice(array.length - count);
    }

    static Sum(array: number[]): number {
        let sum = 0;
        array.forEach((x) => (sum += x));
        return sum;
    }

    static Range(length: number, step: number = 1, include_end: boolean = false, start: number = 0): number[] {
        let arr = [];
        for (let i = start; i < length; i += step) {
            arr.push(i);
        }
        if (include_end) {
            arr.push(length);
        }
        return arr;
    }

    static Unique(array: any[]) {
        function onlyUnique(value:any, index:number, self:any) {
            return self.indexOf(value) === index;
        }


        if(!array) return [];
        return array.filter(onlyUnique);
    }
}

export class FileUtils {
    static images = [
        "jpeg",
        "apng",
        "png",
        "gif",
        "jpg",
        "bmp",
        "ico",
        "svg",
        "tif",
        "tiff",
        "webp",
        "undefined"
    ];
    static videos = ["mp4", "ogg", "webm", "MOV"];
    static files = [
        "pdf",
        "doc",
        "docx",
        "ppt",
        "pptx",
        "xls",
        "xlsx",
        "txt",
        "zip",
        "7z",
    ];

    static isFile(filename: string) {
        const ext = FileUtils.getExtension(filename);
        return FileUtils.files.includes(ext);
    }

    static isImage(filename: string) {
        const ext = FileUtils.getExtension(filename);
        return FileUtils.images.includes(ext);
    }

    static isVideo(filename: string) {
        const ext = FileUtils.getExtension(filename);
        // console.log(ext);
        return FileUtils.videos.includes(ext);
    }

    static getExtension(filename: string) {
        if (!filename || filename.length <= 0) return "";
        else
            return ArrayUtils.takeLast(filename.split("."), 1)[0].toLowerCase();
    }

    static removeExtension(filename: string) {
        if (!filename || filename.length <= 0) return "";
        else
            return filename.split(".")[0];
    }

    static getAttachmentType(filename: string) {
        if (!filename || filename.length <= 0) return null;
        else if (FileUtils.isVideo(filename)) return FileType.Video;
        else if (FileUtils.isImage(filename)) return FileType.Image;
        else if (FileUtils.isFile(filename)) return FileType.File;
        else return null;
    }

    static getAttachmentName(filename: string) {
        if (!filename || filename.length <= 0) return "";
        else return ArrayUtils.takeLast(filename.split("/"), 1)[0];
    }

    static async readAsBase64(file: File): Promise<string> {


        const prom = new Promise<string>((res, rej) => {
            const reader = new FileReader();
            reader.addEventListener("load", (ev) => {
                const base64 = reader.result as string;
                res(base64);
            })
            reader.addEventListener("error", (ev) => {
                rej(null);
            })
            reader.readAsDataURL(file);

        });
        return prom
    }
}
