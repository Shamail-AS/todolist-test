import { IApiProvider } from "../models/api/common";

export class ApiService {

	private _provider:IApiProvider;
	
	constructor(provider:IApiProvider) {
		this._provider = provider;
	}

}

export default class ApiServiceSingleton{

	public getInstance(){
		//return single instance of ApiService
	}
}